require './vehicle_class'

class Car < Vehicle
  def initialize(wheel_num, color, brand, price, is_passcar)
    super(wheel_num, color, brand, price)
    @is_passcar = is_passcar
  end

  attr_accessor :is_passcar

  def display
    super.concat "\tPassenger Car     : #{is_passcar}\n"
  end

  def self.create(amount = 1)
    super amount do |wheels, color, manufacturer, price|
      is_passcar = (0..1) == 1
      Car.new(wheels, color, manufacturer, price, is_passcar)
    end
  end
end
